#include "Quadrado.hpp"


Quadrado::Quadrado(string tipo, float base, float altura){
    set_tipo(tipo);
    set_base(base);
    set_altura(altura);
}
float Quadrado::calcula_area(){
    return (get_altura()*get_base());
}
float Quadrado::calcula_perimetro(){
    return (get_altura()*2)+(get_base()*2);
}