#include "Pentagono.hpp"
#include <cmath>

Pentagono::Pentagono(string tipo, float lado){
    set_tipo(tipo);
    set_base(lado);
}   
float Pentagono::calcula_area(){
    set_altura((get_base()/2)/tan(0.6283));
    return (((get_base()*get_altura())/2)*5);
    }
float Pentagono::calcula_perimetro(){
    return (get_base()*5);
}