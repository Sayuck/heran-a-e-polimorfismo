#include "Paralelogramo.hpp"
#include <cmath>

Paralelogramo::Paralelogramo(string tipo, float ladoa, float altura, float ladob){
    set_altura(altura);
    set_base(ladoa);
    set_tipo(tipo);
    set_ladob(ladob);
}
void Paralelogramo::set_ladob(float ladob){
    this->ladob = ladob;
}
float Paralelogramo::get_ladob(){
    return ladob;
}
float Paralelogramo::calcula_area(){
    return (get_base()*get_altura());
}
float Paralelogramo::calcula_perimetro(){
    return ((get_base()+get_ladob())*2);
}