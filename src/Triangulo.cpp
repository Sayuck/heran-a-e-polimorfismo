#include "formageometrica.hpp"
#include "Triangulo.hpp"
#include <iostream>
#include <cmath>

Triangulo::Triangulo(string tipo, float base, float altura){
    set_tipo(tipo);
    set_base(base);
    set_altura(altura);
}

float Triangulo::calcula_area(){
    return ((get_base()*get_altura())/2);
}

float Triangulo::calcula_perimetro(){
    float hipotenusa = sqrt(pow(get_base(),2) + pow(get_altura(),2));
    return get_altura() + get_base() + hipotenusa;
}