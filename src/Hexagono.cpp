#include "Hexagono.hpp"
#include <cmath>

Hexagono::Hexagono(string tipo, float lado){
    set_base(lado);
    set_tipo(tipo);
    }
float Hexagono::calcula_area(){
    return (3*sqrt(3.0)*pow(get_base(),2))/2;
    }
float Hexagono::calcula_perimetro(){
    return (get_base()*6);
    }