#include "Circulo.hpp"
//#define _USE_MATH_DEFINES
#include <cmath>

Circulo::Circulo(string tipo, float raio){
    set_tipo(tipo);
    set_altura(raio);
}
float Circulo::calcula_area(){
    return ((M_PI)*pow(get_altura(),2));
}
float Circulo::calcula_perimetro(){
    return (2*M_PI*get_altura());
}