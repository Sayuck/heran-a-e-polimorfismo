#include "Circulo.hpp"
#include "Hexagono.hpp"
#include "Paralelogramo.hpp"
#include "Pentagono.hpp"
#include "Quadrado.hpp"
#include "Triangulo.hpp"
#include "formageometrica.hpp"
#include <iostream>
#include <vector>

using namespace std;

int main(){

vector <FormaGeometrica*> lista;
lista.emplace_back(new Quadrado("Quadrado", 15.0,15.0));//certo
lista.emplace_back(new Circulo("Círculo", 5.0));//certo
lista.emplace_back(new Paralelogramo("Paralelogramo Retângulo", 9.0,2.0,3.0));//certo
lista.emplace_back(new Triangulo("Triângulo", 15.0,20.0));//certo
lista.emplace_back(new Pentagono("Pentágono", 7.0));
lista.emplace_back(new Hexagono("Hexágono", 9.0));

for (size_t i=0; i<lista.size(); i++){
    cout << lista[i]->get_tipo() << " Área: "<< lista[i]->calcula_area()<< " Perímetro: "<< lista[i]->calcula_perimetro()<< endl;
}
return 0;
}