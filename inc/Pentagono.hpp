#ifndef PENTAGONO_HPP
#define PENTAGONO_HPP
#include "formageometrica.hpp"

using namespace std;

class Pentagono : public FormaGeometrica {

public:
    Pentagono(string tipo, float lado);
    float calcula_area();
    float calcula_perimetro();
};

#endif