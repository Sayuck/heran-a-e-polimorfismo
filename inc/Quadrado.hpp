#ifndef QUADRADO_HPP
#define QUADRADO_HPP
#include "formageometrica.hpp"
#include <iostream>

using namespace std;

class Quadrado : public FormaGeometrica {

public:
    Quadrado(string tipo, float base, float altura);
    float calcula_area();
    float calcula_perimetro();
};

#endif