#ifndef PARALELOGRAMO_HPP
#define PARALELOGRAMO_HPP
#include "formageometrica.hpp"

using namespace std;

class Paralelogramo : public FormaGeometrica {
private:
    float ladob;

public:
    Paralelogramo(string tipo, float ladoa, float altura, float ladob);
    float calcula_area();
    float calcula_perimetro();
    void set_ladob(float ladob);
    float get_ladob();
};

#endif