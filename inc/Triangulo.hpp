#ifndef TRIANGULO_HPP
#define TRIANGULO_HPP
#include "formageometrica.hpp"

using namespace std;

class Triangulo : public FormaGeometrica {

public:
    Triangulo(string tipo, float base, float altura);
    float calcula_area();
    float calcula_perimetro();
};

#endif