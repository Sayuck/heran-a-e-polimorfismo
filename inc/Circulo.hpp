#ifndef CIRCULO_HPP
#define CIRCULO_HPP
#include "formageometrica.hpp"

using namespace std;

class Circulo : public FormaGeometrica {

public:
    Circulo(string tipo, float raio);
    float calcula_area();
    float calcula_perimetro();
};

#endif