#ifndef HEXAGONO_HPP
#define HEXAGONO_HPP
#include "formageometrica.hpp"

using namespace std;

class Hexagono : public FormaGeometrica {

public:
    Hexagono(string tipo, float lado);
    float calcula_area();
    float calcula_perimetro();
};

#endif