Roberto Martins da Nóbrega mat: 14/0065547

# Exercício de Herança e Polimorfismo - Orientação à Objetos 2019/2

Este exercício consiste em utilizar como base a classe FormaGeometrica disponível neste repositório e criar 6 classes filhas: Triângulo, Quadrado, Círculo, Paralelogramo, Pentágono e Hexágono.

Em cada uma das classes filha, será necessário haver um exemplo de Sobrerga de Método. 

A main de seu programa deverá conter uma lista de tamanho variável que possa conter objetos de todas as classes criadas e ser capaz de imprimir os valores de Área e Perímetro de cada um dos itens da lista de maneira correta a partir do uso da Sobrescrita de Método.


## Procedimento para resolver o exercício

1. Crie um Fork deste repositório;
2. Faça a implementação das 6 classes solicitadas;
3. Crie a main com a lista de objetos;
4. Crie uma lista de objetos de exemplo contento pelo menos um objeto de cada classe;
5. implemente um laço na main que imprima o tipo,a a área e o perímetro de cada objeto corretamente.
